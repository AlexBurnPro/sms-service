package com.example.smsservice.model;

public class Message {

    private String number;
    private String ordinate;
    private String text;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOrdinate() {
        return ordinate;
    }

    public void setOrdinate(String ordinate) {
        this.ordinate = ordinate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Message{" +
                "number='" + number + '\'' +
                ", ordinate='" + ordinate + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
