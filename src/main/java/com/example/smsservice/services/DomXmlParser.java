package com.example.smsservice.services;

import com.example.smsservice.model.Message;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class DomXmlParser {

    private static final String TAG_SMS = "sms";
    private static final String TAG_SERVICE_ID = "service-id";
    private static final String TAG_DESTINATION = "destination";
    private static final String TAG_SOURCE = "source";
    private static final String TAG_UD = "ud";
    private static final String TAG_ADDRESS = "address";
    private static final String TAG_NUMBER = "number";
    private static final String TAG_ORIGINATE = "originate";

    //test
    private static final String SOURCE_XML = "message.xml";


    public void parse() {

        Message message = new Message();
        Document document;
        Node smsNode;
        Node sourceNode = null;

        String text = "";
        String number = "";
        String originate = "";

        try {
            document = buildDocument(SOURCE_XML);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        Node messageNode = document.getFirstChild();
        NodeList messageChild = messageNode.getChildNodes();
        for (int i = 0; i < messageChild.getLength(); i++) {

            if (messageChild.item(i).getNodeType() != Node.ELEMENT_NODE
                    && (!messageChild.item(i).getNodeName().equals(TAG_SMS))) continue;

            smsNode = messageChild.item(i);
            NodeList smsChild = smsNode.getChildNodes();
            for (int k = 0; k < smsChild.getLength(); k++) {

                if (smsChild.item(k).getNodeType() != Node.ELEMENT_NODE) continue;

                switch (smsChild.item(k).getNodeName()) {
                    case TAG_SERVICE_ID: {}
                    case TAG_DESTINATION: {}
                    case TAG_SOURCE: sourceNode = smsChild.item(k);
                        break;
                    case TAG_UD: text = smsChild.item(k).getTextContent();
                        break;
                }

                NodeList sourceChild  = sourceNode.getChildNodes();
                for (int h = 0; h < sourceChild.getLength(); h++) {

                    if (sourceChild.item(h).getNodeType() != Node.ELEMENT_NODE
                            && (!sourceChild.item(h).equals(TAG_ADDRESS))) continue;

                    NodeList addressChild = sourceChild.item(h).getChildNodes();
                    for(int j = 0; j < addressChild.getLength(); j++) {

                        if (addressChild.item(j).getNodeType() != Node.ELEMENT_NODE) continue;

                        switch (addressChild.item(j).getNodeName()) {
                            case TAG_NUMBER: number = addressChild.item(j).getTextContent();
                                break;
                            case TAG_ORIGINATE: originate = addressChild.item(j).getTextContent();
                                break;
                        }
                    }
                }
            }
            message.setNumber(number);
            message.setOrdinate(originate);
            message.setText(text);
        }
    }

    private Document buildDocument(String source) throws Exception {
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        return documentBuilder.parse(source);
    }
}
