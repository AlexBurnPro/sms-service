package com.example.smsservice.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping("/api/xml")
public class SmsController {

    private final Logger logger = LoggerFactory.getLogger(SmsController.class);

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK, reason = "OK")
    public void getXmlRequest() {
        logger.info("Request has been performed");

    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK, reason = "OK")
    public void postXmlRequest() {
        logger.info("Request has been performed");

    }
}
